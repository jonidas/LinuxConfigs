# Vim config
The `.vimrc` file should reside in the users home folder `~`.

Also the `plug.vim` should be in `~/.vim/autoload/` to make sure that the calls to the vim plugin manager in `.vimrc` can be excuted. To do so run `:PlugInstall` in vims command mode. Afterwards the statusbar should show all relevant file infos, including the current git branch name.