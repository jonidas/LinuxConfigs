"m Set compatibility to Vim only.
set nocompatible

" Helps force plug-ins to load correctly when it is turned back on below.
filetype off

" Turn on syntax highlighting.
syntax on

" For plug-ins to load correctly.
filetype plugin indent on

" Turn off modelines
set modelines=0

" Automatically wrap text that extends beyond the screen length.
set wrap
" Vim's auto indentation feature does not work properly with text copied from outside of Vim. Press the <F2> key to toggle paste mode on/off.
nnoremap <F2> :set invpaste paste?<CR>
imap <F2> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F2>

" Status bar
set laststatus=2

" Display options
set showmode
set showcmd

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" Show line numbers
set number relativenumber
set nu rnu

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-fugitive'

call plug#end()

set statusline =

" file infos (name)
set statusline +=%f\ %h%m%r%w

" Filetype
set statusline +=%y

" git branch
set statusline +=\ %{fugitive#statusline()}

" Line, column and percentage
set statusline +=%=%-14.(%l,%c%V%)\ %P
